const express = require('express');
const app = express();

//************************CLASE CELULARES******************************* */
let arrayCelulares = [];

class celular{
    constructor(marca,gama,modelo,pantalla,sistOper,precio){
        this.marca = marca;
        this.gama = gama;
        this.modelo = modelo;
        this.pantalla = pantalla;
        this.sistOper = sistOper;
        this.precio = precio;
    }

    get obtienePrecio(){
        return this.precio;
    }
}

function cargarCelus(){
    
    let celu1 = new celular("Hyundai","Media","Live 2","5.7","Android",15000);
    let celu2 = new celular("LG","Baja","G3","5","Android",8000);
    let celu3 = new celular("Samsung","Alta","S10","7","Android",120000);
    let celu4 = new celular("Samsung","Media","S4","5","Android",100000);
    let celu5 = new celular("Motorola","Alta","M50","7","Android",150000);
    arrayCelulares.push(celu1);
    arrayCelulares.push(celu2);
    arrayCelulares.push(celu3);
    arrayCelulares.push(celu4);
    arrayCelulares.push(celu5);
}

function mostrarTodos(){
    console.log(arrayCelulares);
}
cargarCelus();
mostrarTodos();

//************************FIN CLASE CELULARES******************************* */


//************************ENDPOINTS******************************* */
//Listado completo de celus
app.get('/listado-completo', (req, res) =>{
    res.json(arrayCelulares);
});

//Listado de celus por la mitad
app.get('/listado-mitad', (req, res) =>{
    tamanio = arrayCelulares.length;
    nuevoTamanio = tamanio / 2;
    nuevoArreglo = arrayCelulares.slice(0, nuevoTamanio);
    res.json(nuevoArreglo);
});

//Menor precio
app.get('/menor-precio', (req, res) =>{
    let precioMenor = arrayCelulares[0].precio;
    let pos = 0;
    for(let i=0; i<arrayCelulares.length; i++){
        if(arrayCelulares[i].precio < precioMenor){
            console.log(i);
            precioMenor = arrayCelulares[i].precio;
            pos = i;
        }
    }    
    console.log(pos);
    res.json(arrayCelulares[pos]);
});

//Mayor precio
app.get('/mayor-precio', (req, res) =>{
    let precioMayor = arrayCelulares[0].precio;
    let pos = 0;
    for(let i=0; i<arrayCelulares.length; i++){
        if(arrayCelulares[i].precio > precioMayor){
            console.log(i);
            precioMayor = arrayCelulares[i].precio;
            pos = i;
        }
    }    
    console.log(pos);
    res.json(arrayCelulares[pos]);
});

//Listado agrupado por gamas
app.get('/gamas', (req, res) =>{
    arrayGama = [];
    for(let i=0; i<arrayCelulares.length; i++){
        if(arrayCelulares[i].gama === "Baja"){
            arrayGama.push(arrayCelulares[i]);
        }
    }
    for(let i=0; i<arrayCelulares.length; i++){
        if(arrayCelulares[i].gama === "Media"){
            arrayGama.push(arrayCelulares[i]);
        }
    }
    for(let i=0; i<arrayCelulares.length; i++){
        if(arrayCelulares[i].gama === "Alta"){
            arrayGama.push(arrayCelulares[i]);
        }
    }
    res.json(arrayGama);
});

//************************ FIN ENDPOINTS******************************* */

//activo servidor
let port = 3000;
function activar(){
    app.listen(port, () =>{
        console.log('Servidor iniciado en el puerto ' + port);
    });
}

module.exports = {
    activar
}